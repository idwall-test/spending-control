# API Spending Control

API para controle e recomendação de gastos

## Protosta de solução
v.0.0.1
![ScreenShot](/docs/PropostaDeSolução.drawio.png)

### Amazon API Gateway 
https://aws.amazon.com/pt/api-gateway/
- Fornece uma API personalizada para o cliente
- Adiciona uma camada adicional de segurança no microserviço
- Arquitetado para alta disponibilidade e escala

### Amazon EKS
https://aws.amazon.com/pt/eks/
- Aplicações que aumentam e diminuem a escala na vertical automaticamente
- Possível aproveitar toda a performance, a escala, a confiabilidade e a disponibilidade da infraestrutura da AWS
- O Amazon EKS facilita a atualização dos clusters em execução para a versão mais recente do Kubernetes

### CloudWatch
https://aws.amazon.com/pt/cloudwatch/
- Coleta todos os eventos em log
- Permite uma consulta rapida e criação de dashboards personalizados
- Criação de Alertas

### SQS
https://aws.amazon.com/pt/sqs/
- Fila para gerenciar as mensagem
- Persistencia baseado em evento ASYNC

### Lambda
http://aws.amazon.com/pt/lambda/
- Função Lambda para persistir no banco de dados
- Trigger SQS

### Redis Cache 
https://redis.io/
- Reduzir a largura de banda
- Reduzir a latência
- Reduzir a carga nos servidores
- Ocultar falhas do clint 
- start redis local:
```
sudo service redis-server start
```
teste de conexão
```
redis-cli 
127.0.0.1:6379> ping
PONG
```

### Resilience4j
http://resilience4j.readme.io
- Implementado Feature Retry
- Configurado para 3 tentativas de chamada caso o client(API externa) não responda ou responda com erro

Config:
```
resilience4j:
  retry:
    instances:
      transaction:
        maxRetryAttempts: 3
        waitDuration: 2s
```
Retry API externa:
![ScreenShot](/docs/ResilienceRetry.drawio.png)

## API POC
- Kotlin
- Spring Boot
- Test JUnit + Mockito
- Swagger
  http://localhost:8001/spedingcontrol/swagger-ui/index.html
- MockApi
  https://mockapi.io/projects/62eef0628d7bc7c2eb73ab75

## Repositórios

API POC
https://gitlab.com/idwall-test/spending-control
```
git clone https://gitlab.com/idwall-test/spending-control.git
```

Lambda Function
https://gitlab.com/idwall-test/lambda-persist-response
```
git clone https://gitlab.com/idwall-test/lambda-persist-response.git
```

## Subindo aplicação
Após executar o git clone, dentro do diretorio executar o docker-compose:
```
docker compose up
```

## Autor
Murilo Mendes Carvalho

carvalho.m.murilo@gmail.com
