FROM adoptopenjdk/openjdk11:alpine
EXPOSE 8001
ADD /build/libs/spending-control-0.0.1-SNAPSHOT.jar spending-control.jar
ENTRYPOINT ["java", "-jar", "spending-control.jar"]