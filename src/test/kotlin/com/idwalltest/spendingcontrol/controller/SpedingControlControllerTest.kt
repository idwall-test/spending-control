package com.idwalltest.spendingcontrol.controller

import com.idwalltest.spendingcontrol.model.TransactionDetail
import com.idwalltest.spendingcontrol.model.request.SpedingControlRequest
import com.idwalltest.spendingcontrol.model.response.SpendingControlResponse
import com.idwalltest.spendingcontrol.service.SpedingControlService
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.HttpStatus
import org.springframework.test.context.junit4.SpringRunner
import kotlin.test.assertEquals

@RunWith(SpringRunner::class)
@SpringBootTest
class SpedingControlControllerTest{

    @Mock
    private lateinit var spedingControlService: SpedingControlService

    @InjectMocks
    private lateinit var spedingControlController: SpedingControlController

    @Test
    fun when_find_user_speding_control_transaction_return_ok() {
        val transactionDetailList = listOf(
            TransactionDetail(
            "3",
            "FOOD",
            "3",
            "132.3",
            "01/10/2021 12:30:12"
        ))

        val spendingControlResponse = SpendingControlResponse("2", transactionDetailList)

        val spedingControlRequest = SpedingControlRequest("2", "01/10/2021", "30/10/2021")

        `when`(spedingControlService.spedingControlTransaction(spedingControlRequest))
            .thenReturn(spendingControlResponse)

        val response = spedingControlController.getTransactions(spedingControlRequest)
        assertEquals(HttpStatus.OK, response.statusCode)
    }
}