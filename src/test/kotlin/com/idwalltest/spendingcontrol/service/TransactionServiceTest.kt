package com.idwalltest.spendingcontrol.service

import com.idwalltest.spendingcontrol.gateway.TransactionClient
import com.idwalltest.spendingcontrol.gateway.model.TransactionResponse
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import kotlin.test.assertNotNull
@RunWith(SpringRunner::class)
@SpringBootTest
class TransactionServiceTest{

    @Mock
    private lateinit var transactionClient: TransactionClient

    @InjectMocks
    private lateinit var transactionService: TransactionService
    @Test
    fun when_find_transaction_by_userId_return_success(){
        val userId = "2"
        val transactionResponse = listOf(TransactionResponse(
            "2",
            "3",
            "314.16",
            "12-01-2021 21:10:03"
        ))

        `when`(transactionClient.getTransactionByUserId(userId))
            .thenReturn(transactionResponse)

        val response = transactionService.getTransactionById(userId)

        assertNotNull(response)
        verify(transactionClient, times(1)).getTransactionByUserId(userId)
    }
}