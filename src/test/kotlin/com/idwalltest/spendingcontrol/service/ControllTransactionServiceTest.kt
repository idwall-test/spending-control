package com.idwalltest.spendingcontrol.service

import com.idwalltest.spendingcontrol.gateway.model.TransactionResponse
import com.idwalltest.spendingcontrol.model.ClassifiedCategoryEnum
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@RunWith(SpringRunner::class)
@SpringBootTest
class ControllTransactionServiceTest{

    @InjectMocks
    private lateinit var controlTransactionService: ControlTransactionService
    @Test
    fun control_transaction_success(){
        val transactionResponse = listOf(TransactionResponse(
            "2",
            "1",
            "314.16",
            "2038-10-01T01:10:58.714Z"
        ))

        val response = controlTransactionService.controlTransaction("2", transactionResponse)

        assertNotNull(response)
    }

    @Test
    fun control_transaction_classified_category_equals_EDUCATION(){
        val transactionResponse = listOf(TransactionResponse(
            "1",
            "3",
            "314.16",
            "2038-10-01T01:10:58.714Z"
        ))

        val response = controlTransactionService.controlTransaction("1", transactionResponse)

        assertEquals(ClassifiedCategoryEnum.EDUCATION.name, response.expenses[0].categoryName)
    }

    @Test
    fun control_transaction_purchase_date_formatted(){
        val transactionResponse = listOf(TransactionResponse(
            "2",
            "1",
            "314.16",
            "2021-10-01T12:10:58.714Z"
        ))

        val response = controlTransactionService.controlTransaction("2", transactionResponse)

        assertEquals("01/10/2021 12:10:58", response.expenses[0].purchaseDate)
    }
}