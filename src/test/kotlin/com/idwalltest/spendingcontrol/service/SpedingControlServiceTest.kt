package com.idwalltest.spendingcontrol.service

import com.idwalltest.spendingcontrol.exception.TransactionNotFoundException
import com.idwalltest.spendingcontrol.gateway.model.TransactionResponse
import com.idwalltest.spendingcontrol.model.TransactionDetail
import com.idwalltest.spendingcontrol.model.request.SpedingControlRequest
import com.idwalltest.spendingcontrol.model.response.SpendingControlResponse
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class SpedingControlServiceTest{

    @Mock
    private lateinit var transactionService: TransactionService

    @Mock
    private lateinit var controlTransactionService: ControlTransactionService

    @Mock
    private lateinit var sendMessageService: SendMessageService

    @InjectMocks
    private lateinit var spedingControlService: SpedingControlService

    @Test
    fun when_find_user_transaction_return_and_send_message(){
        val transactionResponse = listOf(TransactionResponse(
            "2",
            "3",
            "314.16",
            "12-01-2021 21:10:03"
        ))

        val transactionDetailList = listOf(TransactionDetail(
            "3",
            "FOOD",
            "3",
            "132.3",
            "01/10/2021 12:30:12"
        ))

        val spendingControlResponse = SpendingControlResponse("2", transactionDetailList)

        `when`(transactionService.getTransactionById("2"))
            .thenReturn(transactionResponse)

        `when`(controlTransactionService.controlTransaction("2", transactionResponse))
            .thenReturn(spendingControlResponse)

        val spedingControlRequest = SpedingControlRequest("2", "01/10/2021", "30/10/2021")

        val response = spedingControlService.spedingControlTransaction(spedingControlRequest)

        assertEquals("2", response.id)
        verify(sendMessageService, times(1)).sendMessagePersistResponse(response)
    }

    @Test
    fun when_not_find_user_transaction_TransactionNotFoundException(){
        val transactionResponse = listOf<TransactionResponse>()

        `when`(transactionService.getTransactionById("2"))
            .thenReturn(transactionResponse)

        assertThrows<TransactionNotFoundException> {
            val spedingControlRequest = SpedingControlRequest("450", "01/10/2021", "30/10/2021")
            spedingControlService.spedingControlTransaction(spedingControlRequest)
        }
    }
}