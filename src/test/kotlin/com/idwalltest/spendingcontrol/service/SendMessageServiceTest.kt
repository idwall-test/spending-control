package com.idwalltest.spendingcontrol.service

import com.amazonaws.services.sqs.AmazonSQS
import com.idwalltest.spendingcontrol.configuration.QueueProperties
import com.idwalltest.spendingcontrol.model.TransactionDetail
import com.idwalltest.spendingcontrol.model.response.SpendingControlResponse
import org.junit.jupiter.api.Test
import org.junit.runner.RunWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest
class SendMessageServiceTest{
    @Mock
    private lateinit var queueProperties: QueueProperties

    @Mock
    private lateinit var amazonSQS: AmazonSQS

    @InjectMocks
    private lateinit var sendMessageService: SendMessageService
    @Test
    fun send_message_persist_response_success(){
        val transactionDetailList = listOf(
            TransactionDetail(
            "3",
            "FOOD",
            "3",
            "132.3",
            "01/10/2021 12:30:12"
        ))

        val spendingControlResponse = SpendingControlResponse("2", transactionDetailList)

        sendMessageService.sendMessagePersistResponse(spendingControlResponse)

        verify(amazonSQS, times(1)).sendMessage(any())
    }
}