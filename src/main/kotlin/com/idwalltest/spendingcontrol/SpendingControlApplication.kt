package com.idwalltest.spendingcontrol

import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cache.annotation.EnableCaching
import org.springframework.cloud.openfeign.EnableFeignClients

@SpringBootApplication
@EnableFeignClients
@EnableCaching
@EnableAutoConfiguration
class SpendingControlApplication
fun main(args: Array<String>) {
	runApplication<SpendingControlApplication>(*args)
}
