package com.idwalltest.spendingcontrol.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("aws")
class AmazonProperties (
    var region: String = "",
    var accesskey: String = "",
    var secretkey: String = ""
)