package com.idwalltest.spendingcontrol.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("redis")
class RedisProperties (
    var host: String = "",
    var port: Int = 0,
    var prefix: String = ""
)