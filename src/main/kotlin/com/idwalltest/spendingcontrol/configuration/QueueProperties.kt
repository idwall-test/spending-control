package com.idwalltest.spendingcontrol.configuration

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.stereotype.Component

@Component
@ConfigurationProperties("queue")
class QueueProperties (
    var persitresponse: String = "",
)