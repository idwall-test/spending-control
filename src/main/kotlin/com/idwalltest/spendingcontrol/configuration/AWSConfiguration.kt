package com.idwalltest.spendingcontrol.configuration

import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.AmazonSQSClient
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class AWSConfiguration (
    private val amazonProperties: AmazonProperties
    ){

    private val amazonRegion: String = amazonProperties.region
    private val amazonAWSAccessKey: String = amazonProperties.accesskey
    private val amazonAWSSecretKey: String = amazonProperties.secretkey
    @Bean
    fun createSQSClient(): AmazonSQS? {
        return AmazonSQSClient.builder()
            .withCredentials(AWSStaticCredentialsProvider(amazonAWSCredentials()))
            .withRegion(amazonRegion)
            .build()
    }
    @Bean
    fun amazonAWSCredentials(): AWSCredentials {
        return BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey)
    }

}