package com.idwalltest.spendingcontrol.gateway.model

data class TransactionResponse(
    var userId: String,
    var purchaseId: String,
    var moneySpent: String,
    var purchaseDate: String
) : java.io.Serializable
