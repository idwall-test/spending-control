package com.idwalltest.spendingcontrol.gateway

import com.idwalltest.spendingcontrol.gateway.model.TransactionResponse
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

@FeignClient(url="https://62eef0628d7bc7c2eb73ab74.mockapi.io/api/v1", name="transaction")
interface TransactionClient {

    @GetMapping("/user/{id}/transaction")
    fun getTransactionByUserId(@PathVariable id: String): List<TransactionResponse>
}