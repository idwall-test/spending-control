package com.idwalltest.spendingcontrol.controller

import com.idwalltest.spendingcontrol.model.request.SpedingControlRequest
import com.idwalltest.spendingcontrol.model.response.SpendingControlResponse
import com.idwalltest.spendingcontrol.service.SpedingControlService
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestBody

@Controller
class SpedingControlController (
    private val spedingControlService: SpedingControlService
) {
    private val log = LoggerFactory.getLogger(javaClass)
    @Operation(summary = "API responsible for returning transactions for expense control", description = "Returns 200 if successful")
    @ApiResponses(
        value = [
            ApiResponse(responseCode = "200", description = "Transaction"),
            ApiResponse(responseCode = "401", description = "Client not found"),
            ApiResponse(responseCode = "404", description = "Transaction not found"),
        ]
    )
    @GetMapping("/v1/transaction", consumes = ["application/json"])
    fun getTransactions(
        @RequestBody spedingControlRequest: SpedingControlRequest
    ): ResponseEntity<SpendingControlResponse> {
        val response = spedingControlService.spedingControlTransaction(spedingControlRequest)
        return ResponseEntity.ok(response)
    }
}