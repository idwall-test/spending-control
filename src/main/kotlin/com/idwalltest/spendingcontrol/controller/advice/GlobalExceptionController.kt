package com.idwalltest.spendingcontrol.controller.advice

import com.idwalltest.spendingcontrol.exception.TransactionNotFoundException
import com.idwalltest.spendingcontrol.model.response.GenericResponse
import feign.FeignException
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class GlobalExceptionController {

    private val log = LoggerFactory.getLogger(javaClass)

    @ExceptionHandler(value = [TransactionNotFoundException::class])
    fun handleClientException(e: TransactionNotFoundException): ResponseEntity<GenericResponse> {
        log.error("GlobalExceptionController -> handleClientException -> ${e.message}", e)
        return ResponseEntity.status(HttpStatus.NOT_FOUND.value()).body(
            GenericResponse(
                code = HttpStatus.NOT_FOUND.value(),
                status = HttpStatus.NOT_FOUND.toString(),
                message = "TRANSACTION NOT FOUND"
            )
        )
    }

    @ExceptionHandler(value = [FeignException::class])
    fun hundleFeignServerException(e: FeignException.FeignServerException) : ResponseEntity<GenericResponse> {
        log.error("GlobalExceptionController -> hundleFeignServerException -> ${e.message}", e)
        return ResponseEntity.status(HttpStatus.UNAUTHORIZED.value()).body(
            GenericResponse(
                code = HttpStatus.UNAUTHORIZED.value(),
                status = HttpStatus.UNAUTHORIZED.toString(),
                message = "CLIENT FOUND"
            )
        )
    }
}