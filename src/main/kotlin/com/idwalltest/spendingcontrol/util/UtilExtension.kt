package com.idwalltest.spendingcontrol.util

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter

fun String.formatDate() : String {
    val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
    val dateTime: LocalDateTime = LocalDateTime.parse(this, formatter)
    return dateTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"))
}