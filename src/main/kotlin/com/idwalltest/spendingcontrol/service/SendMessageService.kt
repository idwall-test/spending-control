package com.idwalltest.spendingcontrol.service

import com.amazonaws.services.sqs.AmazonSQS
import com.amazonaws.services.sqs.model.SendMessageRequest
import com.fasterxml.jackson.databind.ObjectMapper
import com.idwalltest.spendingcontrol.configuration.QueueProperties
import com.idwalltest.spendingcontrol.model.response.SpendingControlResponse
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class SendMessageService(
    private val queueProperties: QueueProperties,
    private val amazonSQS: AmazonSQS
) {
    private val log = LoggerFactory.getLogger(javaClass)
    fun sendMessagePersistResponse(spendingControlResponse: SpendingControlResponse) {
        log.info("SendMessageService - MESSAGE: {}", ObjectMapper().writeValueAsString(spendingControlResponse))
        val sendMessageRequest = SendMessageRequest()
            .withQueueUrl(queueProperties.persitresponse)
            .withMessageBody(ObjectMapper().writeValueAsString(spendingControlResponse))

        amazonSQS.sendMessage(sendMessageRequest)
    }
}