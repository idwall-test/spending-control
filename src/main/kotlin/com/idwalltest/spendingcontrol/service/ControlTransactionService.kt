package com.idwalltest.spendingcontrol.service

import com.idwalltest.spendingcontrol.gateway.model.TransactionResponse
import com.idwalltest.spendingcontrol.model.ClassifiedCategoryEnum
import com.idwalltest.spendingcontrol.model.TransactionDetail
import com.idwalltest.spendingcontrol.model.response.SpendingControlResponse
import com.idwalltest.spendingcontrol.util.formatDate
import org.springframework.stereotype.Service

@Service
class ControlTransactionService {
    fun controlTransaction(userId: String, transactions: List<TransactionResponse>): SpendingControlResponse {

        val transactionDetailList = ArrayList<TransactionDetail>()

        transactions.forEach { t ->
            val classifiedCategory = ClassifiedCategoryEnum.classifiedCategoryById(t.purchaseId)

            transactionDetailList.add(
                TransactionDetail(
                    categoryId = classifiedCategory.categoryId,
                    categoryName = classifiedCategory.categoryName,
                    purchaseId = t.purchaseId,
                    moneySpent = t.moneySpent,
                    purchaseDate = t.purchaseDate.formatDate()
                )
            )
        }

        return SpendingControlResponse(userId, transactionDetailList)
    }
}