package com.idwalltest.spendingcontrol.service

import com.idwalltest.spendingcontrol.exception.TransactionNotFoundException
import com.idwalltest.spendingcontrol.model.request.SpedingControlRequest
import com.idwalltest.spendingcontrol.model.response.SpendingControlResponse
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service

@Service
class SpedingControlService (
    private val transactionService: TransactionService,
    private val controlTransactionService: ControlTransactionService,
    private val sendMessageService: SendMessageService
) {
    private val log = LoggerFactory.getLogger(javaClass)

    fun spedingControlTransaction(spedingControlRequest: SpedingControlRequest): SpendingControlResponse {
        val userId = spedingControlRequest.id

        log.info("SpedingControlService -> spedingControlTransaction - userID: {}", userId)

        val transactionResponse = transactionService.getTransactionById(userId)

        if(transactionResponse.isEmpty()) {
            log.info("SpedingControlService -> transactionResponse is empty - userID: {}", userId)
            throw TransactionNotFoundException()
        }

        val resultSpedingControl = controlTransactionService.controlTransaction(userId, transactionResponse)

        sendMessageService.sendMessagePersistResponse(resultSpedingControl)

        return resultSpedingControl
    }
}