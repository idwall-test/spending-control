package com.idwalltest.spendingcontrol.service

import com.idwalltest.spendingcontrol.gateway.TransactionClient
import com.idwalltest.spendingcontrol.gateway.model.TransactionResponse
import io.github.resilience4j.retry.annotation.Retry
import lombok.AllArgsConstructor
import org.slf4j.LoggerFactory
import org.springframework.cache.annotation.CacheConfig
import org.springframework.cache.annotation.Cacheable
import org.springframework.stereotype.Service

@Service
@AllArgsConstructor
@CacheConfig(cacheNames=["transaction"])
class TransactionService(
   private val transactionClient: TransactionClient
) {
    private val log = LoggerFactory.getLogger(javaClass)

    @Cacheable(value = ["getTransactionById"], key="{#id}")
    @Retry(name = "transaction")
    fun getTransactionById(id: String): List<TransactionResponse> {
        log.info("TransactionService -> getTransactionById: {}", id)
        return transactionClient.getTransactionByUserId(id)
    }
}