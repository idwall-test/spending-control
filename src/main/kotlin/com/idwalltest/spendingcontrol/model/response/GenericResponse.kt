package com.idwalltest.spendingcontrol.model.response

class GenericResponse (
    val code: Int,
    val status: String,
    val message: String
)