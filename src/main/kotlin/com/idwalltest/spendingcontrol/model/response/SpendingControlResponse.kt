package com.idwalltest.spendingcontrol.model.response

import com.idwalltest.spendingcontrol.model.TransactionDetail

data class SpendingControlResponse(
    var id: String,
    val expenses: List<TransactionDetail>
)