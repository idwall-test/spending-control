package com.idwalltest.spendingcontrol.model.request

data class SpedingControlRequest(
    val id: String,
    val initialDate: String,
    val finalDate: String
)
