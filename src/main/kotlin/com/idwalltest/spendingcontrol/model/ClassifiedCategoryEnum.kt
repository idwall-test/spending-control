package com.idwalltest.spendingcontrol.model

enum class ClassifiedCategoryEnum(
    val categoryId: String
    ) {
    FOOD("1"),
    HOUSE("2"),
    EDUCATION("3"),
    LEISURE("4"),
    ELECTRONICS("4"),
    RESTAURANT("5"),
    HEALTH("6"),
    SERVICES("7"),
    TRANSPORT("8"),
    CLOTHING("9"),
    TRAVEL("10"),
    OTHERS("999999");

    companion object {
        fun classifiedCategoryById(categoryId: String) : ClassifiedCategory {
            ClassifiedCategoryEnum.values().forEach { classifiedCategory ->
                if (classifiedCategory.categoryId.equals(categoryId)){
                    return ClassifiedCategory(classifiedCategory.categoryId, classifiedCategory.name)
                }
            }
            return ClassifiedCategory(OTHERS.categoryId, OTHERS.name)
        }
    }
}
