package com.idwalltest.spendingcontrol.model

data class ClassifiedCategory(
    val categoryId: String,
    val categoryName: String
)
