package com.idwalltest.spendingcontrol.model

data class TransactionDetail(
    var categoryId: String,
    var categoryName: String,
    var purchaseId: String,
    var moneySpent: String,
    var purchaseDate: String
)
