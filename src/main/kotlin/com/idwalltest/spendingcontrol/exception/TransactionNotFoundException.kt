package com.idwalltest.spendingcontrol.exception

class TransactionNotFoundException(override val message: String? = "Transaction found") : RuntimeException(message)